const { request, response } = require('express')
const express = require('express')
const router  = express.Router()
const db = require('../db')
const utils = require('../utils')

router.post('/',(request,response)=>{
    const{name,dept,salary} = request.body
    const query = `insert into employee values(default,'${name}','${dept}','${salary}')`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.get('/all',(request,response)=>{
    const query = `select* from employee`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.put('/:id',(request,response)=>{
    const{salary} = request.body
    const{id} = request.params
    const query = `update employee set salary='${salary}' where empid='${id}'`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.delete('/:id',(request,response)=>{
    const{id} = request.params
    const query = `delete from employee where empid='${id}'`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

module.exports = router