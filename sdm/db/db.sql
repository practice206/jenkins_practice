create table employee(
    empid int primary key auto_increment,
    name varchar(30),
    dept varchar(30),
    salary int
);

create table product(
    pid int primary key auto_increment,
    name varchar(30),
    description varchar(30),
    price int
);

