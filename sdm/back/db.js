const mysql = require('mysql2')
const pool = mysql.createPool({
    host:"veejen",
    user:"root",
    password:"root",
    database:"veejen",
    port:3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0,
})

module.exports = pool