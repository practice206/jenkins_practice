const { request, response } = require('express')
const express = require('express')
const router  = express.Router()
const db = require('../db')
const utils = require('../utils')

router.post('/',(request,response)=>{
    const{name,description,price} = request.body
    const query = `insert into product values(default,'${name}','${description}','${price}')`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.get('/all',(request,response)=>{
    const query = `select* from product`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.put('/:id',(request,response)=>{
    const{price} = request.body
    const{id} = request.params
    const query = `update product set price='${price}' where pid='${id}'`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.delete('/:id',(request,response)=>{
    const{id} = request.params
    const query = `delete from product where pid='${id}'`
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

module.exports = router