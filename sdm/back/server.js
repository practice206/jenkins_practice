const express = require('express')
const cors = require('cors')
const app = express()
const emprouter = require('./routes/employee')
const prorouter = require('./routes/product')

app.use(cors('*'))
app.use(express.json())
app.use('/product',prorouter)
app.use('/employee',emprouter)

app.listen(4000, '0.0.0.0',()=>{
    console.log('server started onmpoet 4000')
})